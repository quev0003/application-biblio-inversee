# Bibliographie inversée

Cette application a pour objectif d'extraire des données à partir de Google Scholar relatives aux citations d'un article de référence. En entrant l'identifiant d'un article, l'utilisateur pourra télécharger les données de plusieurs articles ayant cité l'ouvrage (titre, résumé, auteur, année, etc.). Cette outil permet d'obtenir des données au format csv pour une analyse statistique ou encore la constitution d'une bibliographie.

## Prérequis

Pour utiliser cette application, vous devez avoir les packages suivants installés dans R :

    * shiny
    * rvest
    * tidyr
    * dplyr

## Installation

   1. Téléchargez le fichier app.R à partir de ce repository GitLab.
   2. Ouvrez RStudio et ouvrez le fichier app.R.
   3. Installez les packages nécessaires en exécutant les commandes suivantes dans la console R :

```
install.packages("shiny")
install.packages("rvest")
install.packages("tidyr")
install.packages("dplyr")
```

Lancez l'application en cliquant sur le bouton "Run App" dans RStudio ou en exécutant la commande suivante dans la console R :

```
shiny::runApp()
```

## Utilisation

   1. Saisissez l'identifiant de citation de l'article de référence (disponilbe dans l'URL, ex: cites=\_8313213127749369813\_).
   2. Cliquez sur le bouton "Scraper" pour démarrer le processus de scraping.
   3. Les données extraites sont affichées dans le tableau. Vous pouvez les télécharger en cliquant sur le bouton "Télécharger".

## Résultats

L'application extrait les données relatives aux principaux articles ayant cité l'article de référence. Les données peuvent être téléchargées au format csv pour une utilisation ultérieure.
